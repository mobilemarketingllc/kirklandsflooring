<?php get_header(); ?>

<div class="container">
	<div class="row">

		<?php //FLTheme::sidebar( 'left' ); ?>

		<div class="fl-content col-md-12 <?php //FLTheme::content_class(); ?>">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					$video_id = get_field('blogvideoslug', get_the_ID());
					echo $video_id;
					get_template_part( 'content', 'single' );
				endwhile;
			endif;
			?>
		</div>

		<?php //FLTheme::sidebar( 'right' ); ?>

	</div>
</div>

<?php get_footer(); ?>
